<?php
declare(strict_types = 1);

namespace Trick\PageHead\Wrappers;

/**
 * @property-read ?int $fbAppId
 * @property-read ?string $url
 * @property-read ?string $title
 * @property-read ?string $description
 * @property-read ?string $image
 * @property-read ?string $type
 */
class OpenGraphWrapper
{
	use \Nette\SmartObject;

	private ?int $fbAppId = null;

	private ?string $url = null;

	private ?string $title = null;

	private ?string $description = null;

	private ?string $image = null;

	private ?string $type = null;


	public function setUrl(string $url): void
	{
		$this->url = $url;
	}

	protected  function getUrl(): ?string
	{
		return $this->url;
	}

	public function setFbAppId(string $id): void
	{
		$this->fbAppId = (int) $id;
	}

	protected  function getFbAppId(): ?int
	{
		return $this->fbAppId;
	}

	public function setTitle(string $title): void
	{
		$this->title = $title;
	}

	protected  function getTitle(): ?string
	{
		return $this->title;
	}

	public function setDescription(string $description): void
	{
		$this->description = $description;
	}

	protected  function getDescription(): ?string
	{
		return $this->description;
	}

	public function setImage(string $image): void
	{
		$this->image = $image;
	}

	protected  function getImage(): ?string
	{
		return $this->image;
	}

	public function setType(string $type): void
	{
		$this->type = $type;
	}

	public function getType(): ?string
	{
		return $this->type;
	}

}
