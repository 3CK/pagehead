<?php
declare(strict_types = 1);

namespace Trick\PageHead\Wrappers;

/**
 * @property-read string $themeColor
 * @property-read string $msTileColor
 * @property-read string $safaryTileColor
 * @property-read bool $isSvg
 */
class FaviconWrapper
{
	use \Nette\SmartObject;

	private string $themeColor = '#fff';

	private string $msTileColor = '#fff';

	private string $safaryTileColor = '#fff';

	private bool $isSvg = false;

	public function setThemeColor(string $color): void
	{
		$this->themeColor = $color;
	}

	public function getThemeColor(): string
	{
		return $this->themeColor;
	}

	public function setMsTileColor(string $color): void
	{
		$this->msTileColor = $color;
	}

	public function getMsTileColor(): string
	{
		return $this->msTileColor;
	}

	public function setSafaryTileColor(string $color): void
	{
		$this->safaryTileColor = $color;
	}

	public function getSafaryTileColor(): string
	{
		return $this->safaryTileColor;
	}

	public function setIsSvg(bool $yesNo = true): void
	{
		$this->isSvg = $yesNo;
	}

	public function getIsSvg(): bool
	{
		return $this->isSvg;
	}
}
