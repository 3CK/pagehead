<?php
declare(strict_types = 1);

namespace Trick\PageHead\Wrappers;

/**
 * @property-read bool $show
 * @property-read ?string $type
 * @property-read ?string $url
 * @property-read ?string $name
 * @property-read ?string $phone
 * @property-read ?string $email
 * @property-read ?string $image
 * @property-read array $address
 * @property-read ?array $openingHours
 */
class RichSnippetsWrapper
{
	use \Nette\SmartObject;

	private ?string $type = null;

	private ?string $url = null;

	private ?string $name = null;

	private array $address = [];

	private ?array $openingHours = [];

	private ?string $phone = null;

	private ?string $email = null;

	private ?string $image = null;


	protected function isShow(): bool
	{
		return (bool) $this->type;
	}

	public function setType(string $type): void
	{
		$this->type = $type;
	}

	protected  function getType(): ?string
	{
		return $this->type;
	}

	public function setUrl(string $url): void
	{
		$this->url = $url;
	}

	protected  function getUrl(): ?string
	{
		return $this->url;
	}

	public function setName(string $name): void
	{
		$this->name = $name;
	}

	protected  function getName(): ?string
	{
		return $this->name;
	}

	public function setPhone(string $phone): void
	{
		$this->phone = $phone;
	}

	protected  function getPhone(): ?string
	{
		return $this->phone;
	}

	public function setEmail(string $email): void
	{
		$this->email = $email;
	}

	protected  function getEmail(): ?string
	{
		return $this->email;
	}

	public function setImage(string $image): void
	{
		$this->image = $image;
	}

	protected  function getImage(): ?string
	{
		return $this->image;
	}

	public function setAddress(array $address): void
	{
		$this->address = $address;
	}

	protected  function getAddress(): array
	{
		return $this->address;
	}

	public function setOpeningHours(array $hours): void
	{
		foreach ($hours as $key => $item) {
			if (isset($item['closed'])) {
				unset($hours[$key]);
			}
		}

		$this->openingHours = $hours;
	}

	public function getOpeningHours(): ?array
	{
		return $this->openingHours;
	}
}
