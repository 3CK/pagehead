<?php
declare(strict_types = 1);

namespace Trick\PageHead;

use Trick\PageHead\Wrappers\FaviconWrapper;
use Trick\PageHead\Wrappers\OpenGraphWrapper;
use Trick\PageHead\Wrappers\RichSnippetsWrapper;

class PageHead extends \Nette\Application\UI\Control
{
	/**
	 * Title
	 */

	private string $title = '';

	private bool $appendTitleSuffix = true;

	private string $titleSuffix = '';

	/**
	 * SEO
	 */

	private bool $index = true;

	private bool $follow = true;

	private ?string $description = null;

	/**
	 * CSS
	 */

	private array $styles = [];

	private ?string $criticalCss = null;

	/**
	 * Javascripts
	 */

	private array $scripts = [];

	private bool $useAos = false;

	/**
	 * Favicon
	 */

	private FaviconWrapper $favicon;

	/**
	 * Miscellaneous
	 */

	private string $author = '';

	private RichSnippetsWrapper $richSnippets;

	private OpenGraphWrapper $openGraph;

	private string $basePath = '';

	private ?string $googleTagManagerId = null;


	public function __construct()
	{
		$this->richSnippets = new RichSnippetsWrapper;
		$this->openGraph = new OpenGraphWrapper;
		$this->favicon = new FaviconWrapper;
	}



	public function render()
	{
		$this->template->title = $this->title;
		if ($this->appendTitleSuffix) {
			$this->template->title .= ' ' . $this->titleSuffix;
		}

		$this->template->index = $this->index ? 'index' : 'noindex';
		$this->template->follow = $this->follow ? 'follow' : 'nofollow';
		$this->template->author = $this->author;
		$this->template->gtmId = $this->googleTagManagerId;
		$this->template->description = $this->description;
		$this->template->styles = $this->styles;
		$this->template->criticalCss = $this->criticalCss;
		$this->template->scripts = $this->scripts;
		$this->template->useAos = $this->useAos;
		$this->template->richSnippet = $this->richSnippets;
		$this->template->og = $this->openGraph;
		$this->template->favicon = $this->favicon;

		$templateFile = dirname($this->getReflection()->getFileName()) . '/templates/pagehead.latte';
		$this->template->setFile($templateFile);
		$this->template->render();
	}


	public function setTitle(string $title): void
	{
		$this->title = $title;
		$this->openGraph->setTitle($title);
	}


	public function appendTitleSuffix(bool $yesNo): void
	{
		$this->appendTitleSuffix = $yesNo;
	}


	public function setTitleSuffix(string $titleSuffix): void
	{
		$this->titleSuffix = $titleSuffix;
	}


	public function setIndex(bool $yesNo): void
	{
		$this->index = $yesNo;
	}


	public function setFollow(bool $yesNo): void
	{
		$this->follow = $yesNo;
	}


	public function setDescription(string $description): void
	{
		$this->description = $description;
		$this->openGraph->setDescription($description);
	}


	public function setGoogleTagManagerId(string $id): void
	{
		$this->googleTagManagerId = $id;
	}


	public function setAuthor(string $author): void
	{
		$this->author = $author;
	}


	public function addStyle(string $stylePath): void
	{
		if (!$this->isAbsoluteUrl($stylePath)) {
			if (is_file(WWW_DIR . $stylePath)) {
				$mtime = date('U', filemtime(WWW_DIR . $stylePath));
				$stylePath = $this->basePath . $stylePath . '?v=' . $mtime;
				$this->styles[] = $stylePath;
			}
		} else {
			$this->styles[] = $stylePath;
		}
	}

	public function getStyles(): array
	{
		return $this->styles;
	}

	public function addCriticalStyle(string $filePath): void
	{
		$this->criticalCss = file_get_contents($filePath);
	}

	public function addScript(string $scriptPath): void
	{
		if (!$this->isAbsoluteUrl($scriptPath)) {
			if (is_file(WWW_DIR . $scriptPath)) {
				$mtime = date('U', filemtime(WWW_DIR . $scriptPath));
				$scriptPath = $this->basePath . $scriptPath . '?v=' . $mtime;
				$this->scripts[] = $scriptPath;
			}
		} else {
			$this->scripts[] = $scriptPath;
		}
	}

	public function getScripts(): array
	{
		return $this->scripts;
	}

	public function useAos(bool $yesNo): void
	{
		$this->useAos = $yesNo;
	}

	public function getRichSnippets(): RichSnippetsWrapper
	{
		return $this->richSnippets;
	}

	public function getOpenGraph(): OpenGraphWrapper
	{
		return $this->openGraph;
	}

	public function getFavicon(): FaviconWrapper
	{
		return $this->favicon;
	}

	private function isAbsoluteUrl(string $url): bool
	{
		return (bool) preg_match('$^https?://$', $url);
	}

	public function setBasePath(string $path): void
	{
		$this->basePath = $path;
	}
}
